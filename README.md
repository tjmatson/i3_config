i3 configs
==========

Contains i3wm and i3blocks config files, plus random scripts for i3blocks.  
The various scripts require arp-scan, pywnoaa, and an Arch Linux system.

pywnoaa (a weather script) can be found at: https://gitlab.com/tjmatson/pywnoaa

Inconsolata is used as the main font, with FontAwesome for icons.

Installation
------------

```
$ git clone git@gitlab.com:tjmatson/i3_config.git
$ mv bin ~
$ mv i3blocks ~/.config
$ mv i3/config ~/.i3
```

You'll most likely want to choose specific pieces of the configs, rather than  
overwriting what you have.